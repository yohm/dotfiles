# auto change directory
#
setopt auto_cd

# auto directory pushd that you can get dirs list by cd -[tab]
#
setopt auto_pushd

# command correct edition before each completion attempt
#
setopt correct

# compacked complete list display
#
setopt list_packed

# no remove postfix slash of command line
#
setopt noautoremoveslash

# no beep sound when complete list displayed
#
setopt nolistbeep

# emacs key bind
#
bindkey -e

# completion configuration
fpath=(/usr/local/share/zsh-completions $fpath)
autoload -U compinit
compinit -u
# zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' '+m:{A-Z}={a-z}'

autoload -Uz vcs_info
zstyle 'vcs_info:*' formats '[%b]'
zstyle 'vcs_info:*' actionformats '[%b|%a]'

#### history
HISTFILE="$HOME/.zhistory"
HISTSIZE=10000
SAVEHIST=10000

#### option, limit, bindkey
setopt  hist_ignore_all_dups
setopt  hist_reduce_blanks
setopt  share_history

bindkey '^P' history-beginning-search-backward
bindkey '^N' history-beginning-search-forward

## Alias configuration
#
# expand aliases before completing
#
setopt complete_aliases # aliased ls needs if file/dir completions work

alias where="command -v"
alias j="jobs -l"
alias vi="vim"
alias o="open"
alias ql='qlmanage -p "$@" >& /dev/null'

case "${OSTYPE}" in
freebsd*|darwin*)
  alias ls="ls -G"
  ;;
linux*)
  alias ls="ls --color"
  ;;
esac

alias la="ls -a"
alias lf="ls -F"
alias ll="ls -lh"

alias rm="rm -i"
alias mv="mv -i"
alias cp="cp -i"
function rmtil() {rm -f ./*~; rm -f ./*.bak; rm -f ./.*~}
alias rmo="rm ./*.o"

alias du="du -h"
alias df="df -h"

alias su="su -l"

alias more="less"

# ls after cd
#
function cd() { builtin cd $@ && ls -G; }

# change encoding into euc.
#
function euc() {
    for i in $@; do;
        nkf -e -Lu $i >! /tmp/euc.$$
        mv -f /tmp/euc.$$ $i
    done;
}

# change encoding into sjis.
function sjis() {
    for i in $@; do;
        nkf -s -Lw $i >! /tmp/euc.$$
        mv -f /tmp/euc.$$ $i
    done;
}

# change encoding into utf-8
function utf8() {
    for i in $@; do;
        nkf -w8 $i >! /tmp/euc.$$
        mv -f /tmp/euc.$$ $i
    done;
}


local WATER=$'%{\e[0;36m%}'
local PURPLE=$'%{\e[0;35m%}'
local BLUE=$'%{\e[0;34m%}'
local YELLOW=$'%{\e[0;33m%}'
local GREEN=$'%{\e[0;32m%}'
local RED=$'%{\e[0;31m%}'
local BLACK=$'%{\e[0;30m%}'
local GLAY=$'%{\e[0;37m%}'

local DEFAULT=$'%{\e[1;00m%}' #http://www.machu.jp/b/zsh.html

export PROMPT=$WATER'[${USER}@ ${HOST}] '$DEFAULT
export RPROMPT='%1(v|%F{green}%1v%f|)'$YELLOW'[%~]'$DEFAULT
setopt PROMPT_SUBST

export LSCOLORS=di=32:ln=35:ex=31:bd=47
export LSCOLORS=cxfxgxdxbxegedabagacad

## mount network drive
fmount () {
    echo "mounting $1"
    osascript -e "tell application \"Finder\" to mount volume \"$1\""
}

# colorize the completion list
zstyle ':completion:*' list-colors $LSCOLORS

# for bundler
alias be="bundle exec"

# print current directory to the title
precmd () {
  echo -ne "\e]2;${PWD}\a"
  echo -ne "\e]1;${PWD:t}\a"

  psvar=()
  LANG=en_US.UTF-8 vcs_info
  [[ -n "$vcs_info_msg_0_" ]] && psvar[1]="$vcs_info_msg_0_"
}

disable r

function exists { which $1 &> /dev/null }

if exists peco; then
    function peco_select_history() {
        local tac
        exists gtac && tac="gtac" || { exists tac && tac="tac" || { tac="tail -r" } }
        BUFFER=$(history -n 1 | eval $tac | peco --query "$LBUFFER")
        CURSOR=$#BUFFER         # move cursor
        zle -R -c               # refresh
    }

    zle -N peco_select_history
    bindkey '^R' peco_select_history
fi

function git-hash(){                                    
    git log --oneline --branches | peco | awk '{print $1}'
} 
alias -g H='$(git-hash)'
function gim() {
      vim `git ls-files | peco`
}
alias g="git"

export PATH=/usr/local/bin:$PATH
export PATH=/usr/texbin:$PATH

export JAVA_HOME="`/usr/libexec/java_home -v '1.7*'`"

# export EDITOR=/Applications/MacVim.app/Contents/MacOS/Vim
alias mvim='env LANG=ja_JP.UTF-8 /Applications/MacVim.app/Contents/MacOS/mvim "$@"'

if which rbenv > /dev/null; then eval "$(rbenv init -)"; fi
if which pyenv > /dev/null; then eval "$(pyenv init -)"; fi

export LC_ALL=ja_JP.UTF-8
export LANG=ja_JP.UTF-8

## load user .zshrc configuration file
#
[ -f ~/.zshrc.mine ] && source ~/.zshrc.mine

pandoc_embed_html () {
  pandoc --self-contained -s --mathjax=https://gist.githubusercontent.com/yohm/0c8ed72b6f18948a2fd3/raw/624defc8ffebb0934ab459854b7b3efc563f6efb/dynoload.js -c https://gist.githubusercontent.com/griffin-stewie/9755783/raw/13cf5c04803102d90d2457a39c3a849a2d2cc04b/github.css $@
}

# added by travis gem
[ -f /Users/murase/.travis/travis.sh ] && source /Users/murase/.travis/travis.sh
