syntax on
highlight LineNr ctermfg=darkyellow
set number
set tabstop=2 shiftwidth=2 softtabstop=2
set expandtab
set smartindent

" Note: Skip initialization for vim-tiny or vim-small.
if !1 | finish | endif

if has('vim_starting')
  set nocompatible               " Be iMproved

  " Required:
  set runtimepath+=~/.vim/bundle/neobundle.vim/
endif

" Required:
call neobundle#begin(expand('~/.vim/bundle/'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" My Bundles here:
" Refer to |:NeoBundle-examples|.
" Note: You don't set neobundle setting in .gvimrc!
NeoBundle 'Shougo/unite.vim'
NeoBundle 'Shougo/neomru.vim'
NeoBundle 'scrooloose/nerdtree'
NeoBundle 'altercation/vim-colors-solarized'
NeoBundle 'derekwyatt/vim-scala'
NeoBundle 'plasticboy/vim-markdown'
let g:vim_markdown_folding_disabled=1
NeoBundle 'kannokanno/previm'
NeoBundle 'tyru/open-browser.vim'
augroup PrevimSettings
    autocmd!
    autocmd BufNewFile,BufRead *.{md,mdwn,mkd,mkdn,mark*} set filetype=markdown
augroup END
NeoBundle 'sophacles/vim-processing'
NeoBundle 'evandrewry/vim-x10'
if has('gui_macvim')
  NeoBundle 'Valloric/YouCompleteMe', { 'build': {'mac': './install.sh --clang-completer --system-libclang'} }
end
NeoBundle 'leafgarland/typescript-vim'
NeoBundle 'Shougo/vimproc.vim', {
\ 'build' : {
\     'windows' : 'tools\\update-dll-mingw',
\     'cygwin' : 'make -f make_cygwin.mak',
\     'mac' : 'make -f make_mac.mak',
\     'linux' : 'make',
\     'unix' : 'gmake',
\    },
\ }
NeoBundle 'Quramy/tsuquyomi'

call neobundle#end()

let g:ycm_path_to_python_interpreter = '/usr/bin/python'
let g:ycm_filetype_whitelist = { 'cpp': 1 }

" Required:
filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck

set background=dark
colorscheme solarized

""""""""""""""""""""""""""""
" Setup for Unite.vim
let g:unite_enable_start_insert=1
" バッファ一覧
noremap <C-P> :Unite buffer<CR>
" ファイル一覧
noremap <C-N> :Unite -buffer-name=file file<CR>
" 最近使ったファイルの一覧
noremap <C-Z> :Unite file_mru<CR>
" sourcesを「今開いているファイルのディレクトリ」とする
noremap :uff :<C-u>UniteWithBufferDir file -buffer-name=file<CR>
" ウィンドウを分割して開く
au FileType unite nnoremap <silent> <buffer> <expr> <C-J> unite#do_action('split')
au FileType unite inoremap <silent> <buffer> <expr> <C-J> unite#do_action('split')
" ウィンドウを縦に分割して開く
au FileType unite nnoremap <silent> <buffer> <expr> <C-K> unite#do_action('vsplit')
au FileType unite inoremap <silent> <buffer> <expr> <C-K> unite#do_action('vsplit')
" ESCキーを2回押すと終了する
au FileType unite nnoremap <silent> <buffer> <ESC><ESC> :q<CR>
au FileType unite inoremap <silent> <buffer> <ESC><ESC> <ESC>:q<CR>
""""""""""""""""""""""""""""""

" escape from insert mode
inoremap <silent> jj <ESC>

set imdisable

if has('gui_macvim')
  set lines=60 columns=200
  set guifont=Andale\ Mono:h14
  set guifontwide=ヒラギノ角ゴ\ Pro\ W3:h14
endif

au BufRead,BufNewFile *.md set filetype=markdown

set formatoptions=q

set noswapfile

nnoremap j gj
nnoremap k gk
nnoremap <Down> gj
nnoremap <Up>   gk

set directory=/tmp
set backupdir=/tmp
