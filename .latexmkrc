#!/usr/bin/perl
$latex         = 'latex %O %S';
$bibtex        = 'bibtex %O %B';
# $dvipdf        = 'dvipdf %O %S';
$dvipdf        = 'dvipdf %O %S';
$pdflatex      = 'pdflatex %O %S';
$pdf_mode      = 1; # use 3 dvipdf
$pdf_update_method = 0;
$pdf_previewer = "open -a preview.app";
